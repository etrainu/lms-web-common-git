package com.etrainu.tags.utils

import java.text.SimpleDateFormat

class DatePickerTagLib {
	def datePicker = { attrs ->
		def name = attrs['name'] ?  attrs['name'] : 'datePicker_' + ( ( new java.util.Date() ).time * ( new java.util.Random() ).next(2) ).toString() ;
		def id = attrs['id'] ? attrs['id'] : name;
		def icon = attrs['icon'] ? attrs['icon'] : 'gear';
		def value = attrs['value'] ? attrs['value'] : "No Date";
		def buttonid = 'open' + id

		out << '<input type="button" id="' + buttonid + '"'
		out << ' value="' + value + '"'
		out << ' data-icon="' + icon + '"'
		out << ' data-inline="true"'
		out << ' onClick="' + "\$('#" + id + "').focus();" + '" />'
		
		out << '<input type="text"'
		out << ' name="' + name + '"'
		out << ' id="' + id + '"' 
		out << ' value="' + value + '"'
		out << ' data-type="date"'
		out << ' style="display:none;"'
		out << ' onChange="$(' + "'#" + buttonid + "').text(this.value).parent().find('.ui-btn-text').html(this.value);" + '" />'
	}
	
	def datePickerForm = { attrs ->
		def controller = attrs['controller']
		def action = attrs['action']
		def id = attrs['id']
		def fromDateValue = attrs['fromDateValue']
		def toDateValue = attrs['toDateValue']
		def suppressForm = attrs['suppressForm']
		
		if( suppressForm && suppressForm.length() ) {
			try {
				suppressForm = suppressForm.toBoolean() 
			} catch(Throwable e) {
				suppressForm = false
			}
		} else {
			suppressForm = false
		}
		
		def blockA = '<div data-theme="c" style="width: 175px; float: left;">' //Cell 1
		def blockB = '<div data-theme="c" style="width: 200px; float: left;">' //Cell 2
		def blockC = '<div data-theme="c" style="width: 185px; float: left;">' //Cell 3
		def blockD = '<div data-theme="c" style="width: 75px; float: left;">' //Cell 2
		def labelFrom = '<div style="float:left; margin-top:20px; height: 100%;">From:</div>'
		def labelTo = '<div style="float:left; margin-top:20px; height: 100%;">To:</div>'
		def inlineDiv = '<div style="float:right;">'
		def cDiv = '</div>'
		
		def now = Calendar.getInstance()
		
		def tomorrow = now.clone()
		tomorrow.add( Calendar.DAY_OF_MONTH , 1)
		
		def firstOfMonth = now.clone()
		firstOfMonth.set( Calendar.DAY_OF_MONTH , 1 )
		
		def firstOfLastMonth = firstOfMonth.clone()
		firstOfLastMonth.add( Calendar.MONTH, -1 )
		
		def thisMonday = now.clone()
		thisMonday.set( Calendar.DAY_OF_WEEK, Calendar.MONDAY )
		
		def lastMonday = thisMonday.clone()
		lastMonday.add( Calendar.WEEK_OF_MONTH, -1 )
		
		def df = new SimpleDateFormat("dd-MM-yyyy")
		
		out << "<script>"
		out << {
			def StringBuffer out = new StringBuffer();
			
			out.append "\$('.dateRange').live('change', function() {"
			out.append "if(\$('.dateRange').val() == 'today'){"
			out.append "today_click();"
			out.append "return null;"
			out.append "}"
			out.append "if(\$('.dateRange').val() == 'lastWeek'){"
			out.append "lastWeek_click();"
			out.append "return null;"
			out.append "}"
			out.append "if(\$('.dateRange').val() == 'lastMonth'){"
			out.append "lastMonth_click();"
			out.append "return null;"
			out.append "}"
			out.append "});"
						
			out.append "function lastMonth_click() {"
			out.append "setInputValueByName('fromDate', '" + df.format( firstOfLastMonth.getTime() ) + "');"
			out.append "setInputValueByName('toDate', '" + df.format( firstOfMonth.getTime() ) + "');"
			out.append "return true;" 
			out.append "}"
			
			out.append "function today_click() {"
			out.append "setInputValueByName('fromDate', '" + df.format( now.getTime() ) + "');"
			out.append "setInputValueByName('toDate', '" + df.format( tomorrow.getTime() ) + "');"
			out.append "return true;" 
			out.append "}"
			
			out.append "function lastWeek_click() {"
			out.append "setInputValueByName('fromDate', '" + df.format( lastMonday.getTime() ) + "');"
			out.append "setInputValueByName('toDate', '" + df.format( thisMonday.getTime() ) + "');"
			out.append "return true;" 
			out.append "}"
			
			return out.toString()
		}.call()
		
		out << {
			def StringBuffer out = new StringBuffer();
			out.append "function setInputValueByName(name, val) {"
			out.append 'var selector = "input[name=\'"+name+"\']";'
			out.append "\$(selector).attr('value',val).change();"
			out.append "return true;" 
			out.append "}"
			
			return out.toString()
		}.call()
		out << "</script>"
		
		//open form
		if( !suppressForm ) {
			out << '<form action="' + createLink([controller:controller, action:action, id:id]) + '" method="POST" data-ajax="false">'

		} 
		
		out << blockA
		out << '<select name="range" id="range" class="dateRange">'
		out << '<option value="today">Today</option>'
		out << '<option value="lastWeek">Last Week</option>'
		out << '<option value="lastMonth">Last Month</option>'
		out << '</select>'
		out << cDiv
		
		out << blockB
		out << labelFrom
		out << inlineDiv
		out << datePicker([name:"fromDate", value:fromDateValue])
		out << cDiv + cDiv
		
		out << blockC
		out << labelTo
		out << inlineDiv
		out << datePicker([name:"toDate", value:toDateValue])
		out << cDiv + cDiv
		
		if( !suppressForm ) {
			out << blockD
			//To make this button smaller, add data-inline="true" to the html
			//Note: It will be left aligned
			out << '<input type="submit" data-inline="true" id="searchButton" value="Apply" data-icon="refresh" />'
			out << cDiv
		}		
		
		out << cDiv
		
		if( !suppressForm )
			out << '</form>'
	}
}
