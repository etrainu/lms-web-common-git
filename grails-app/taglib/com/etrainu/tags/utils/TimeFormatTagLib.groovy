package com.etrainu.tags.utils

class TimeFormatTagLib {
	def totalTime = {attrs ->

		def time = attrs["time"]
		def dayString = ''
		def hourString = ''
		def minuteString = ''
		
		//output minutes
		if(time > 0 && time < 60){
			minuteString = time + "m"
			out << minuteString
		}
		 
		//output hours
		else if (time >= 60 && time < 1440){
			if (time.mod(60) == 0) {
				hourString = time.intdiv(60) + "h"
				out << hourString
			}
			else {
				hourString = time.intdiv(60) + "h "
				minuteString = time.mod(60) + "m"
				out << hourString + minuteString
			}
		}
		
		//output days
		else if (time >= 1440) {
			dayString = time.intdiv(24).intdiv(60) + "d "
			hourString = (time.intdiv(60)).mod(24) + "h "
			
			//if hours are not null and minutes are null
			if ((time.intdiv(60)).mod(24) != 0 && time.mod(60) == 0) {
				hourString = (time.intdiv(60)).mod(24) + "h"
			}
			
			//if minutes are not null
			if (time.mod(60) != 0) {
				minuteString = time.mod(60) + "m"
			}
			
			//if days are not null and hours and minutes are null
			if(((time.intdiv(60)).mod(24) == 0) && (time.mod(60) == 0)) {
				dayString = time.intdiv(24).intdiv(60) + "d"
				hourString = '';
			}
			out << dayString + hourString + minuteString
		}
		
		//output not available
		else {
			out << "NA"
		}

	}
}