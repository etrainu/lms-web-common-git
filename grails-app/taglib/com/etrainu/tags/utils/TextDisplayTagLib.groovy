package com.etrainu.tags.utils

class TextDisplayTagLib {
	def defaultText = { attrs ->
		def value = attrs["value"]
		def defaultValue = attrs["default"]
		def defaultBlank = false
		def defaultClass = attrs["defaultClass"]
		def acceptedClass = attrs["acceptedClass"]
		def format = attrs["format"]
		
		def test = false
		def useTest = false
		
		//Let the default value be nothing if the value is of 0 length
		if( defaultValue == null ) {
			defaultValue = value
		}
		if( attrs.containsKey("defaultBlank") ) {
			defaultBlank = attrs["defaultBlank"].toBoolean()
		}
		if( attrs.containsKey("test") ) {
			useTest = true
			try {
				test = attrs["test"].toBoolean()
			} catch(e) {
				test = false
			}
		}
		if( !defaultClass ) {
			defaultClass = ""
		}
		
		out << '<span class="'
		if( 
			( !useTest && ( value == null || (defaultBlank && value.length() == 0 ) ) )
			|| (useTest && test)
		 ) {
			out << defaultClass
			out << '">'
			out << defaultValue
		} else {
			out << acceptedClass
			out << '">'
			if( format && value instanceof Date ) {
				out << value.format(format)
			} else {
				out << value
			}
		}
		out << "</span>"
		
	}
}
