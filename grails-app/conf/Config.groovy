// configuration for plugin testing - will not be included in the plugin zip

springcache {
	defaults {
		// set default cache properties that will apply to all caches that do not override them
		eternal = false
		diskPersistent = false
	}
	caches {
		groupCache {
			// set any properties unique to this cache
			memoryStoreEvictionPolicy = "LRU"
			maxElementsInMemory = 1000
			timeToLive = 240 // 4 minutes
		}
	}
}

 
log4j = {
    // Example of changing the log pattern for the default console
    // appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}

    error  'org.codehaus.groovy.grails.web.servlet',  //  controllers
           'org.codehaus.groovy.grails.web.pages', //  GSP
           'org.codehaus.groovy.grails.web.sitemesh', //  layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping', // URL mapping
           'org.codehaus.groovy.grails.commons', // core / classloading
           'org.codehaus.groovy.grails.plugins', // plugins
           'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate'

    warn   'org.mortbay.log'
}
