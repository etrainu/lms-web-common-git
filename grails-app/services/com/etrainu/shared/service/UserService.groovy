package com.etrainu.shared.service

import groovy.util.ConfigObject
import groovy.xml.MarkupBuilder

import org.codehaus.groovy.grails.commons.ConfigurationHolder

import com.etrainu.user.User
import com.etrainu.user.UserActivity
import com.etrainu.user.UserDetails
import com.etrainu.utils.APIClient
import com.etrainu.utils.SearchResult

class UserService {
	
	final private ConfigObject config = ConfigurationHolder.config
	def securityService
	
	static transactional = false
	
	def getUser(String userId) {
		def user = User.parseXML( new APIClient(config.userActionApiString,[id:userId], securityService.getCredentials()).getXml() )
		return user
	}
	
	def getUserForAuthentication(String userName) {
		def user = User.parseXML( new APIClient(config.securityUserForAuthenticationApiString,[user:userName], securityService.getCredentials()).getXml() )
		return user
	}
	
	def authenticateUser(String username, String password) {
		def key = new APIClient(config.securityAuthenticationApiString,[user:username,pass:password], securityService.getCredentials()).getXml()
		
		//TODO: Handle Errors here
		
		return key.text()
	}
	
	def archiveUser(String userId) {
		new APIClient(config.userArchiveApiString,[id:userId], securityService.getCredentials()).getXml()
	}
	
	def unarchiveUser(String userId) {
		new APIClient(config.userUnarchiveApiString,[id:userId], securityService.getCredentials()).getXml()
	}
	
	def getUserActivity(String userId, String fromDate, String toDate) {
		final def xml = new APIClient(config.userActivityApiString,[id:userId,fromDate:fromDate,toDate:toDate], securityService.getCredentials()).getXml()
		
		final activities = []
		xml.userActivity.each {
			activities.add( UserActivity.parse(it) )
		}
		
		return activities
	}
	
	def getPartnerships(String userId) {
		final def xml = new APIClient(config.userPartnershipApiString,[user:userId], securityService.getCredentials()).getXml()
		def partnerships = new ArrayList()
	
		//TODO: Write a partnership class
		xml.partnership.each {
			def partnership = [
				suborgID:it.groupID,
				suborgName:it.groupName,
				deptID:it.middleGroupID,
				deptName:it.middleGroupName,
				orgID:it.primaryGroupID,
				orgName:it.primaryGroupName
			]
	
			partnerships << partnership
		}
		
		return partnerships
	}
	
	def updateUser(Map properties) {
		def writer = new StringWriter()
		def builder = new MarkupBuilder(writer)
		
		// TODO: add a toXML to user.groovy
		// TODO: change param to user object
		// TODO: change usercntroller.groovy to update from a map then output user.toXML
		builder.user()
		{
			id(properties.id)
			firstname(properties.firstname)
			lastname(properties.lastname)
			email(properties.email)
			street(properties.street)
			city(properties.city)
			postcode(properties.postcode)
			phone(properties.phone)
			mobile(properties.mobile)
			fax(properties.fax)
			dob( properties.dob )
		}
		
		new APIClient(config.userActionApiString, writer.toString(), securityService.getCredentials()).postXml()
	}
	
	def updateUserPermissions(String userId, String permissionString) {
		def writer = new StringWriter()
		def builder = new MarkupBuilder(writer)

		builder.user()
		{
			id(userId)
			permissions(permissionString)
		}
		
		new APIClient(config.userRoleEditPermissionsApiString, writer.toString(), securityService.getCredentials()).postXml()
	}
	
	def searchUsers(String searchString, String userType, String archived, String groupId, Integer maxResults) {
		if( groupId == null ) {
			groupId = 0;
		}
		final def users = []
		final def xml = new APIClient(config.userSearchApiString,[
			searchString:searchString,
			type:userType,
			archived:archived,
			gid:groupId,
			maxResults:maxResults
		], securityService.getCredentials()).getXml()
		
		def totalUsers = Integer.parseInt( xml.attributes().get("totalResults") )
				
		xml.user.each {
			def userDetails = UserDetails.parseXML(it)
			users.add userDetails
		}
		
		return new SearchResult(users, totalUsers)
	}
}
