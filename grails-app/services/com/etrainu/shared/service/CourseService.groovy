package com.etrainu.shared.service

import com.etrainu.course.CourseDetails
import com.etrainu.utils.APIClient
import groovy.util.ConfigObject

import org.codehaus.groovy.grails.commons.ConfigurationHolder

import com.etrainu.course.Course
import com.etrainu.course.CourseDetails
import com.etrainu.user.CourseActivity
import com.etrainu.user.CourseActivity.CourseActivityType
import com.etrainu.utils.SearchResult

class CourseService {
	
	final private ConfigObject config = ConfigurationHolder.config
	def securityService
	
	static transactional = false
	
	def getCourse(String courseId) {
		final def xml = new APIClient(config.courseGetApiString,[id: courseId], securityService.getCredentials() ).getXml()
		final Course course = Course.parseXML( xml )
		
		println course

		return course
	}
	
	def searchCourses(String searchString, String includeArchived) {
		def courses = []
		
		final def xml = new APIClient(config.courseSearchApiString,[searchString: searchString, includeArchived:includeArchived, maxResults:config.api.maxResultsDisplayed ], securityService.getCredentials()).getXml()
		final def totalCourses =  Integer.parseInt( xml.attributes().get("totalResults") )
		xml.course.each { courses.add CourseDetails.parseXML(it) }
		
		return new SearchResult(courses, totalCourses)
	}
	
	def getActivity(String courseId, String fromDate, String toDate) {
		final def xml = new APIClient(config.courseActivityApiString,[id:courseId,fromDate:fromDate,toDate:toDate], securityService.getCredentials()).getXml()
		final activities = []
		xml.courseActivity.each {
			final CourseActivity ua = new CourseActivity()
				ua.courseActivityType = CourseActivityType.valueOf(it.@activityType.text())
				ua.courseActivityDate = Date.parse("yyyy-MM-dd",it.@activityDate.text())
				ua.courseActivityCount = it.@activityCount.text().toInteger()
				activities.add(ua)
		}
		
		return activities
	}
	
	def archiveCourse(String courseId) {
		new APIClient(config.courseArchiveApiString,[id:courseId], securityService.getCredentials()).getXml()
	}
	
	def unarchiveCourse(String courseId) {
		new APIClient(config.courseUnarchiveApiString,[id:courseId], securityService.getCredentials()).getXml()
	}
}
