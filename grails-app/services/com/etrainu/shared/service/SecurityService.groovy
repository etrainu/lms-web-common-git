package com.etrainu.shared.service


class SecurityService {
	//This username/password combination is to identify ETRAINU ONLY. it is NOT to be documented
	private final static String APIUsername = 'etrainuAPI'
	private final static String APIPassword = 'eRt]UN_7!$1'

	def springSecurityService

	def getCredentials()
	{
		def resp = [u:APIUsername,p:APIPassword]
		withPrincipal { principal ->
			resp = [key:principal.getApiToken(),u:APIUsername,p:APIPassword]
		}

		return resp
	}

	//Just in case any other details need to be added
	private def withPrincipal(Closure c) {
		if( springSecurityService ) {
			def principal = springSecurityService.getPrincipal()
			//Principal is a string when its the anonymous user
			if( principal && !(principal instanceof String) ) {
				c.call principal
			}
		}
	}
}
