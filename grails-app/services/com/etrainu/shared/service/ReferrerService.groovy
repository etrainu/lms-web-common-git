package com.etrainu.shared.service

import java.util.Map;

import groovy.util.ConfigObject;
import groovy.xml.MarkupBuilder
import org.codehaus.groovy.grails.commons.ConfigurationHolder

import com.etrainu.utils.APIClient
import com.etrainu.referrer.Referrer
import com.etrainu.referrer.ReferrerDetails
import com.etrainu.referrer.ReferrerActivity
import com.etrainu.referrer.ReferrerActivity.ReferrerActivityType

class ReferrerService {
	
	final private ConfigObject config = ConfigurationHolder.config
	def securityService

	static transactional = false
		
	def getReferrer(String referrerId) {
		
		if (referrerId)
		{
			final def xml = new APIClient(config.referrerSearchApiString,[id: referrerId], securityService.getCredentials()).getXml()
			if (xml && xml.referrer && xml.referrer.size())
			{
				final Referrer referrer = Referrer.parse(xml.referrer[0])
				return referrer
			}
		}
		//Could not find the Referrer
		return null
	}
	
	def searchReferrers(String name, String includeArchived) {
		def referrers = []
		final def xml = new APIClient(config.referrerSearchApiString,[name: name, includeArchived:includeArchived], securityService.getCredentials()).getXml()
		xml.referrer.each { referrers.add ReferrerDetails.parseXML(it) }
		return referrers
	}
	
	def getSalesActivity(String referrer, String fromDate, String toDate) {
		final def xml = new APIClient(config.referrerActivityApiString,[id:referrer,fromDate:fromDate,toDate:toDate], securityService.getCredentials()).getXml()
		final activities = []
		xml.referrerActivity.each {
			final ReferrerActivity ua = ReferrerActivity.parse(it)
			activities.add(ua)
		}
		return activities		
	}
	
	def archiveReferrer(String referrerId) {
		new APIClient(config.referrerArchiveApiString,[id:referrerId], securityService.getCredentials()).getXml()
	}
	
	def unarchiveReferrer(String referrerId) {
		new APIClient(config.referrerUnarchiveApiString,[id:referrerId], securityService.getCredentials()).getXml()
	}
	
	def updateReferrer(Referrer referrer) {
		new APIClient(config.referrerActionApiString, referrer.toXML(), securityService.getCredentials()).postXml()
	}

}
