package com.etrainu.shared.service

import com.etrainu.course.CourseDetails
import com.etrainu.utils.APIClient
import groovy.util.ConfigObject

import org.codehaus.groovy.grails.commons.ConfigurationHolder

import com.etrainu.course.Course
import com.etrainu.course.CourseDetails
import com.etrainu.sales.SalesActivity;
import com.etrainu.user.CourseActivity
import com.etrainu.user.CourseActivity.CourseActivityType
import com.etrainu.utils.APIClient
import com.etrainu.utils.SearchResult

class SalesService {
	
	final private ConfigObject config = ConfigurationHolder.config
	def securityService
	
	static transactional = false
	
	def getActivity(String fromDate, String toDate) {
		final def xml = new APIClient(config.salesActivityApiString,[fromDate:fromDate,toDate:toDate], securityService.getCredentials()).getXml()
		final activityMapByCourse = [:]
		
		//Building a map of course : list of sales activities 
		//I am injecting a counter i to that loop
		int i=0
		xml.product.each { p->
			final Course c = new Course(id:p.@id, name:p.@name )
			final activities = []
			xml.product[i].saleActivity.each { 
				final SalesActivity sa = SalesActivity.parse(it)
				sa.inductionId = c.id
				sa.inductionName = c.name
				activities.add(sa)
			}
			activityMapByCourse.put(c, activities)
			i++
		}
		return activityMapByCourse
	}

}
