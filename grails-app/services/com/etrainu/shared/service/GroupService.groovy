package com.etrainu.shared.service

import grails.plugin.springcache.annotations.Cacheable
import groovy.util.ConfigObject

import org.codehaus.groovy.grails.commons.ConfigurationHolder

import com.etrainu.utils.APIClient

class GroupService {
	
	final private ConfigObject config = ConfigurationHolder.config
	def securityService

	static transactional = false
	
	@Cacheable("groupCache")
    def getGroups() 
	{
		getGroups(null)
	}
	
	@Cacheable("groupCache")
	def getGroups(String id)
	{
		final def xml = new APIClient(config.groupListApiString, id ? ["id":id] : [:], securityService.getCredentials()).getXml() 

		final List groups = []
		xml.group.each {
			def group = [:]
			
			group.put("id", it.id.text() )
			group.put("groupName", it.groupName.text() )
			group.put("archiveDate", it.archiveDate.text() )
			
			groups << group
		}
		
		return groups
    }
}
