package com.etrainu.referrer

import groovy.util.slurpersupport.NodeChild

import com.etrainu.utils.ValidationResponse

class Referrer {

	String id
	String domain
	String name
	Double discount
	Boolean isPercentage
	Boolean isDiscount

	def validate() {
		def result = new ValidationResponse();
		if( this.domain == null || this.domain.length() == 0) {
			result.hasErrors = true;
			result.errorMessage = 'Property \'Domain\' cannot be empty.';
		}
		if( this.name == null || this.name.length() == 0) {
			result.hasErrors = true;
			result.errorMessage = 'Property \'Name\' cannot be empty.';
		}
		if( !this.isDiscount ) {
			this.isPercentage = null;
			this.discount = null;
		}

		return result;
	}

	String toXML() {
		final StringWriter XMLResponse = new StringWriter( )
		final def builder = new groovy.xml.MarkupBuilder(XMLResponse)
		this.toXMLNode builder

		return XMLResponse.toString()
	}

	void toXMLNode(groovy.xml.MarkupBuilder builder) {
		builder.referrer(id:this.id) {
			domain(this.domain)
			name(this.name)
			discount(this.discount)
			isDiscount(this.isDiscount)
			isPercentage(this.isPercentage)
		}
	}

	def update(Map props) {
		this.update props, false
	}

	def update(Map props, Boolean nullMissing) {
		if( props.containsKey('id') ) {
			this.id = props.id
		} else if (nullMissing) {
			this.id = null
		}
		if( props.containsKey('domain') ) {
			this.domain = props.domain
		} else if (nullMissing) {
			this.domain = null
		}
		if( props.containsKey('name') ) {
			this.name = props.name
		} else if (nullMissing) {
			this.name = null
		}
		if( props.containsKey('discount') ) {
			this.discount = Double.parseDouble( props.discount )
		} else if (nullMissing) {
			this.discount = null
		}
		if( props.containsKey('isPercentage') ) {
			this.isPercentage = props.isPercentage.toBoolean()
		} else if (nullMissing) {
			this.isPercentage = null
		}
		if( props.containsKey('isDiscount') ) {
			this.isDiscount = props.isDiscount.toBoolean()
		} else if (nullMissing) {
			this.isDiscount = null
		}
	}
	
	def update(NodeChild xml) {
		if( xml.@id.text() )			this.id = xml.@id.text()
		if( xml.name.text() ) 			this.name = xml.name.text()
		if( xml.domain.text() ) 		this.domain = xml.domain.text()
		if( xml.discount.text() ) 		this.discount = Double.parseDouble( xml.discount.text() )
		if( xml.isDiscount.text() ) 	this.isDiscount = xml.isDiscount.text().toBoolean()
		if( xml.isPercentage.text() ) 	this.isPercentage = xml.isPercentage.text().toBoolean()
	}

	def static parse(Map props) {
		def referrer = new Referrer()
		referrer.update props

		return referrer
	}
	
	def static parse(NodeChild xml) {
		def referer = new Referrer()
		referer.update xml
		
		return referer
	}
	
}
