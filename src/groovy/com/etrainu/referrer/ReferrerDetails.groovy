package com.etrainu.referrer

import grails.converters.XML
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date

class ReferrerDetails {
	
	String id
	String domain
	String name
	Double discount
	Boolean isPercentage
	Boolean isDiscount
	Date archiveDate
	int totalPurchase
	Double totalAmount
	Date firstPurchase
	Date lastPurchase
	
	static def parseXML(String xml) {
		def referrerDetailsXML = XML.parse( xml )
		return parseXML(referrerDetailsXML)
	}
	
	static def parseXML(Object xml) {
		ReferrerDetails referrerDetails = new ReferrerDetails()
		
		if( xml.@id.text() )				referrerDetails.id = xml.@id.text()
		if( xml.domain.text() )				referrerDetails.domain = xml.domain.text()
		if( xml.name.text() )				referrerDetails.name = xml.name.text()
		if( xml.discount.text() )			referrerDetails.discount = xml.discount.text().toString().toDouble()
		if( xml.isPercentage.text() )		referrerDetails.isPercentage = xml.isPercentage.text().toString().toBoolean()
		if( xml.isDiscount.text() )			referrerDetails.isDiscount = xml.isDiscount.text().toString().toBoolean()
		if( xml.totalPurchase.text() )		referrerDetails.totalPurchase = xml.totalPurchase.text().toString().toInteger()
		if( xml.totalAmount.text() )		referrerDetails.totalAmount = xml.totalAmount.text().toString().toDouble()
		
		DateFormat df = new SimpleDateFormat("yyyy-M-dd")
		
		if( xml.archiveDate.text() ) {
			String archiveDateData = xml.archiveDate.text()
			referrerDetails.archiveDate = df.parse( archiveDateData )
		}
		
		if( xml.firstPurchase.text() ) {
			String firstPurchaseData = xml.firstPurchase.text()			
			referrerDetails.firstPurchase = df.parse( firstPurchaseData )
		}
		
		if( xml.lastPurchase.text() ) {
			String lastPurchaseData = xml.lastPurchase.text()
			referrerDetails.lastPurchase = df.parse( lastPurchaseData )
		}
				
		return referrerDetails
	}
}
