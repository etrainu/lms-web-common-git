package com.etrainu.referrer

import groovy.util.slurpersupport.NodeChild;

class ReferrerActivity {
	
	def public static final enum ReferrerActivityType {
		SALES
	}
	
	Date referrerActivityDate
	Integer referrerActivityCount
	Double referrerActivityIncome
	ReferrerActivityType referrerActivityType
	
	def static parse(NodeChild xml) {
		def referrerActivity = new ReferrerActivity()
		referrerActivity.update xml
		
		return referrerActivity
	}

	def update(NodeChild xml) {
		if( xml.@activityType.text() ) 		this.referrerActivityType = ReferrerActivityType.valueOf(xml.@activityType.text())
		if( xml.@activityDate.text() ) 		this.referrerActivityDate = Date.parse("yyyy-MM-dd",xml.@activityDate.text())
		if( xml.@activityCount.text() ) 	this.referrerActivityCount = xml.@activityCount.text().toInteger()
		if( xml.@activityIncome.text() ) 	this.referrerActivityIncome = xml.@activityIncome.text().toDouble()
	}
}
