package com.etrainu.user

import groovy.util.slurpersupport.NodeChild;

import java.util.Date;

class UserActivity {

	def public static final enum UserActivityType {
		SCORM_ASSESSMENT,
		ASSESSMENT, 
		LOGIN,
		COURSE_ASSIGNMENT,
		MARKING_COMPETENT,
		PURCHASE
	}
	
	Date userActivityDate
	Integer userActivityCount
	UserActivityType userActivityType
	
	def static parse(NodeChild xml) {
		def userActivity = new UserActivity()
		userActivity.update xml
		
		return userActivity
	}

	def update(NodeChild xml) {
		if( xml.@activityType.text() ) 		this.userActivityType = UserActivityType.valueOf(xml.@activityType.text())
		if( xml.@activityDate.text() ) 		this.userActivityDate = Date.parse("yyyy-MM-dd",xml.@activityDate.text())
		if( xml.@activityCount.text() ) 	this.userActivityCount = xml.@activityCount.text().toInteger()
	}
}
