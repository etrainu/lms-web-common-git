package com.etrainu.user

class CourseActivity {

	def public static final enum CourseActivityType {
		COURSE_ASSIGNMENT,
		COURSE_COMPLETION
	}
	
	Date courseActivityDate
	Integer courseActivityCount
	CourseActivityType courseActivityType
	
}
