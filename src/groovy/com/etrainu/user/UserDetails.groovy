package com.etrainu.user

import grails.converters.XML

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date

class UserDetails {
	String id
	String username
	String firstname
	String lastname
	String email
	Integer groupId
	Integer participantGroupId
	Date archiveDate
	Date lastLogin
	Integer loginCount12Months
	String groupData
	String searchData
	String city
	String state
	Integer courseCount
	Date firstCourse
	Date mostRecentCourse
	String hierarchy
	Integer completedCourses
	Date dateCreated
	Float balance
	
	static def parseXML(String xml) {
		def userDetailsXML = XML.parse( xml )
		return parseXML(userDetailsXML)
	}
	
	static def parseXML(Object xml) {
		UserDetails userDetails = new UserDetails()
		
		if( xml.id.text() )				userDetails.id = xml.id.text()
		if( xml.username.text() )		userDetails.username = xml.username.text()
		if( xml.firstname.text() )		userDetails.firstname = xml.firstname.text()
		if( xml.lastname.text() ) 		userDetails.lastname = xml.lastname.text()
		if( xml.email.text() )			userDetails.email = xml.email.text()
		if( xml.groupId.text() )		userDetails.groupId = Integer.parseInt( xml.groupId.text() )
		if( xml.participantGroupId.text() )
										userDetails.participantGroupId = Integer.parseInt( xml.participantGroupId.text() )
		if( xml.loginCount12Months.text() )	
			userDetails.loginCount12Months = Integer.parseInt( xml.loginCount12Months.text() )
		if( xml.groupData.text() ) 		userDetails.groupData = xml.groupData.text()
		if( xml.searchData.text() )		userDetails.searchData = xml.searchData.text()
		if( xml.city.text() )			userDetails.city = xml.city.text()
		if( xml.state.text() )			userDetails.state = xml.state.text()
		if( xml.courseCount.text() )	userDetails.courseCount = Integer.parseInt( xml.courseCount.text() )
		if( xml.completedCourses.text() ) userDetails.completedCourses = Integer.parseInt( xml.completedCourses.text() )
		if( xml.hierarchy.text() )		userDetails.hierarchy = xml.hierarchy.text()
		if( xml.balance.text() )		userDetails.balance = Float.parseFloat( xml.balance.text() )
		
		DateFormat df = new SimpleDateFormat("yyyy-M-dd")
		if( xml.archiveDate.text() ) {
			String archiveData = xml.archiveDate.text()
			
			userDetails.archiveDate = df.parse( archiveData )
		}
		if( xml.lastLogin.text() ) {
			String lastLoginData = xml.lastLogin.text()
			
			userDetails.lastLogin = df.parse( lastLoginData )
		}
		if( xml.firstCourse.text() ) {
			String firstCourseData = xml.firstCourse.text()
			
			userDetails.firstCourse = df.parse( firstCourseData )
		}
		if( xml.mostRecentCourse.text() ) {
			String mostRecentCourseData = xml.mostRecentCourse.text()
			
			userDetails.mostRecentCourse = df.parse( mostRecentCourseData )
		}
		if( xml.dateCreated.text() ) {
			String dateCreatedData = xml.dateCreated.text()
			
			userDetails.dateCreated = df.parse( dateCreatedData )
		}
		
		return userDetails
	}
}
