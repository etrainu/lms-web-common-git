package com.etrainu.user

import grails.converters.XML

import java.text.DateFormat
import java.text.SimpleDateFormat

class User
{	
	String username
	String firstname
	String lastname
	String password
	String id
	String email
	String street
	String city
	String postcode
	String phone
	String mobile
	String fax
	Date dob
	Boolean active
	Boolean acceptedTerms
	Boolean canArchive
	Boolean hideAds
	Integer groupId
	Date archiveDate
	String permissions = ''
	
	static def parseXML(String xml) {
		def userXML = XML.parse( xml )
		return parseXML(userXML)
	}
	
	static def parseXML(Object xml) {
		User user = new User()
		
		if( xml.id.text() )			user.id = xml.id.text()
		if( xml.username.text() )	user.username = xml.username.text()
		if( xml.password.text() ) 	user.password = xml.password.text()
		if( xml.firstname.text() )	user.firstname = xml.firstname.text()
		if( xml.lastname.text() ) 	user.lastname = xml.lastname.text()
		if( xml.password.text() ) 	user.password = xml.password.text()
		if( xml.email.text() )		user.email = xml.email.text()
		if( xml.street.text() )		user.street = xml.street.text()
		if( xml.city.text() )		user.city = xml.city.text()
		if( xml.postcode.text() )	user.postcode = xml.postcode.text()
		if( xml.phone.text() )		user.phone = xml.phone.text()
		if( xml.mobile.text() )		user.mobile = xml.mobile.text()
		if( xml.fax.text() )		user.fax = xml.fax.text()
		if( xml.active.text() )		user.active = xml.active.text()
		if( xml.canArchive.text() )	user.canArchive = xml.canArchive.text()
		if( xml.hideAds.text() )	user.hideAds = xml.hideAds.text()
		if( xml.groupId.text() )	user.groupId = Integer.parseInt( xml.groupId.text() )
		if( xml.permissions.text()) user.permissions = xml.permissions.text()
		if( xml.acceptedTerms.text() )
			user.acceptedTerms = xml.acceptedTerms.text()
			
		DateFormat df = new SimpleDateFormat("yyyy-M-dd")
		if( xml.dob.text() ) {
			String dobData = xml.dob.text()

			user.dob = df.parse( dobData )
		}
		if( xml.archiveDate.text() ) {
			String archiveData = xml.archiveDate.text()
			
			user.archiveDate = df.parse( archiveData )
		}
		
		return user
	}
}