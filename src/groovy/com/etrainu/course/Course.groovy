package com.etrainu.course

import grails.converters.XML
import groovy.util.slurpersupport.NodeChild;

class Course {
	
	//course id 
	String id
	String href
	String name
	String desc
	String price
	String accessDate
	String status
	//Induction keys
	String key
	String url
	
	//Course is used in hash mapped and sets lets make sure we have sensible equals and hashcode methods
	boolean equals(o) {
		if (this.is(o)) return true;
		if (!o || getClass() != o.class) return false;
	
		Course that = (Course) o;
		if (id? ! id.equals(that.id) : that.id!= null) return false;
	
		return true;
	}
	
	int hashCode() {
		int result = (id ? id.hashCode() : 0);
	}
	
	static def parseXML(String xml) {
		def courseXML = XML.parse( xml )
		return parseXML(courseXML)
	}
	
	static def parseXML(Object xml) {
		Course course = new Course()
		course.update xml
		
		return course
	}
	
	def update(NodeChild xml) {
		if( xml.@id.text() )		this.id = xml.@id.text()
		if( xml.href.text() )		this.href = xml.href.text()
		if( xml.name.text() ) 		this.name = xml.name.text()
		if( xml.desc.text() )		this.desc = xml.desc.text()
		if( xml.price.text() ) 		this.price = xml.price.text()
		//since there appears to be a bit of doubling up around this
		if( xml.cost.text() )		this.price = xml.cost.text()
		if( xml.accessDate.text() ) this.accessDate = xml.accessDate.text()
		if( xml.status.text() )		this.status = xml.status.text()
		if( xml.key.text() )		this.key = xml.key.text()
		if( xml.url.text() )		this.url = xml.url.text()
	}
}
