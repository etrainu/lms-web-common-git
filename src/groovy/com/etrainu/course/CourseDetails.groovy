package com.etrainu.course

import grails.converters.XML

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date

class CourseDetails {
	String id
	String inductionName
	String courseCode
	Boolean isStandalone
	Boolean isScorm
	Boolean feedbackRequired
	Double cost
	Boolean freeKey
	Boolean autoActivate
	Boolean autoMarkCompetent
	Integer participantCount
	Integer completedCount
	Integer deactivatedCount
	Integer expiredCount
	Integer certificateCount
	Integer averageCompletionDays
	Integer averageCompletionMinutes
	Date firstEnrolment
	Date lastEnrolment
	Integer categoryCount
	Boolean customPricing
	Integer bundleCount
	Integer trainingPlanCount
	String searchData
	Date archiveDate
	String supplier
	
	static def parseXML(String xml) {
		def userDetailsXML = XML.parse( xml )
		return parseXML(userDetailsXML)
	}
	
	static def parseXML(Object xml) {
		CourseDetails courseDetails = new CourseDetails()
		
		if( xml.id.text() )					courseDetails.id = xml.id.text()
		if( xml.inductionName.text() )		courseDetails.inductionName = xml.inductionName.text()
		if( xml.courseCode.text() )			courseDetails.courseCode = xml.courseCode.text()
		
		if( xml.participantCount.text() )	courseDetails.participantCount = Integer.parseInt( xml.participantCount.text() )
		if( xml.completedCount.text() )		courseDetails.completedCount = Integer.parseInt( xml.completedCount.text() )
		if( xml.deactivatedCount.text() )	courseDetails.deactivatedCount = Integer.parseInt( xml.deactivatedCount.text() )
		if( xml.expiredCount.text() )		courseDetails.expiredCount = Integer.parseInt( xml.expiredCount.text() )
		if( xml.certificateCount.text() )	courseDetails.certificateCount = Integer.parseInt( xml.certificateCount.text() )
		if( xml.bundleCount.text() )		courseDetails.bundleCount = Integer.parseInt( xml.bundleCount.text() )
		if( xml.trainingPlanCount.text() )	courseDetails.trainingPlanCount = Integer.parseInt( xml.trainingPlanCount.text() )
		if( xml.categoryCount.text() )		courseDetails.categoryCount = Integer.parseInt( xml.categoryCount.text() )
		if( xml.averageCompletionDays.text() )	
											courseDetails.averageCompletionDays = Integer.parseInt( xml.averageCompletionDays.text() )
		if( xml.averageCompletionMinutes.text() )
											courseDetails.averageCompletionMinutes = Integer.parseInt( xml.averageCompletionMinutes.text() )								
		if( xml.cost.text() )				courseDetails.cost = xml.cost.text().toString().toDouble()
		
		if( xml.customPricing.text() )		courseDetails.customPricing = xml.customPricing.text().toString().toBoolean()
		if( xml.isStandalone.text() )		courseDetails.isStandalone = xml.isStandalone.text().toString().toBoolean()
		if( xml.isScorm.text() )			courseDetails.isScorm = xml.isScorm.text().toString().toBoolean()
		if( xml.feedbackRequired.text() )	courseDetails.feedbackRequired = xml.feedbackRequired.text().toString().toBoolean()
		if( xml.freeKey.text() )			courseDetails.freeKey = xml.freeKey.text().toString().toBoolean()
		if( xml.autoActivate.text() )		courseDetails.autoActivate = xml.autoActivate.text().toString().toBoolean()
		if( xml.autoMarkCompetent.text() )	courseDetails.autoMarkCompetent = xml.autoMarkCompetent.text().toString().toBoolean()
		if( xml.supplier.text() ) courseDetails.supplier = xml.supplier.text()
		
		DateFormat df = new SimpleDateFormat("yyyy-M-dd")
		if( xml.archiveDate.text() ) {
			String archiveData = xml.archiveDate.text()
			
			courseDetails.archiveDate = df.parse( archiveData )
		}
		if( xml.firstEnrolment.text() ) {
			String firstEnrolmentData = xml.firstEnrolment.text()
			
			courseDetails.firstEnrolment = df.parse( firstEnrolmentData )
		}
		if( xml.lastEnrolment.text() ) {
			String lastEnrolmentData = xml.lastEnrolment.text()
			
			courseDetails.lastEnrolment = df.parse( lastEnrolmentData )
		}
		
		return courseDetails
	}
	
}
