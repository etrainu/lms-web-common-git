package com.etrainu.sales

import groovy.util.slurpersupport.NodeChild;

class SalesActivity {
	
	Date salesActivityDate
	Integer salesActivityCount
	Double salesActivityIncome
	String inductionId
	String inductionName
	
	def static parse(NodeChild xml) {
		def salesActivity = new SalesActivity()
		salesActivity.update xml
		return salesActivity
	}

	def update(NodeChild xml) {
		if( xml.@inductionName.text() ) 	this.inductionName = xml.@inductionName.text()
		if( xml.@inductionId.text() ) 	this.inductionId = xml.@inductionId.text()
		if( xml.@salesActivityDate.text() ) 	this.salesActivityDate = Date.parse("yyyy-MM-dd",xml.@salesActivityDate.text())
		if( xml.@salesActivityCount.text() ) 	this.salesActivityCount = xml.@salesActivityCount.text().toInteger()
		if( xml.@salesActivityIncome.text() ) 	this.salesActivityIncome = xml.@salesActivityIncome.text().toDouble()
	}
}
