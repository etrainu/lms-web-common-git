package com.etrainu.utils

class ValidationResponse {
	String errorMessage
	boolean hasErrors = false;
	
	def public String toString() {
		return '[ hasErrors : ' + this.hasErrors.toString() + ', errorMessage : ' + this.errorMessage + ']'
	}
}
