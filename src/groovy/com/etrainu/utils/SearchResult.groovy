package com.etrainu.utils

import java.util.List;

class SearchResult {
	
	public Integer maxResults
	public List results
	
	SearchResult(List results, Integer maxResults) {
		this.results = results
		this.maxResults = maxResults
	}
	
	SearchResult(List results) {
		this(results, results.size())
	}
	
	SearchResult() {
		this([])
	}
}
