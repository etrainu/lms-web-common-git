package com.etrainu.utils

import groovy.lang.Closure

import org.apache.commons.logging.LogFactory

class MonitoringUtils {
	
	private static final log = LogFactory.getLog(this)
	
	public static def benchmark(String methodName, Closure closure) {
		
		final start = System.currentTimeMillis()
		final def result = closure.call()
		final now = System.currentTimeMillis()
		log.info "${methodName} execution took ${now - start} ms"
		return result
	  }

}
