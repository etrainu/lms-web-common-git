package com.etrainu.utils

import groovy.util.slurpersupport.NodeChild
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.RESTClient
import org.apache.commons.logging.LogFactory
import org.codehaus.groovy.grails.commons.ApplicationHolder as AH

class APIClient extends RESTClient {
	
	//def securityService
	
	private static final log = LogFactory.getLog(this)
	private Map<String,?> args
	
	def APIClient(){ }
	
	def APIClient(String uri, Map<String,?> credentials) {
		this(uri,[:], credentials)
	}
	
	def APIClient(String uri, String xml, Map<String,?> credentials) {
		this(uri,[xml:xml], credentials)
	}
	
	def APIClient(String uri, Map<String,?> args, Map<String,?> credentials) {
		this.setUri( uri )
		this.setHeaders( credentials )
		this.setArgs( args )
	}
	
	public Object get(Map<String,?> args) {
		MonitoringUtils.benchmark ("APIClient.get(${this.getUri()})") {
			return super.get(args)
		}
	}
	
	public Object post(Map<String,?> args) {
		MonitoringUtils.benchmark ("APIClient.post(${this.getUri()})") {
			//TODO: When we figure out what is wrong with RESTClient here, we will use that instead of this hack
			// Which circumvents the wrapper class that is RESTClient
			def http = new HTTPBuilder( this.getUri() )
			http.setHeaders( this.getHeaders() )
			return http.post(body:[args])
		}
	}
	
	public NodeChild getXml() {
		def resp = this.get([query:this.args,requestContentType:"XML"])
		return resp.data
	}
	
	public NodeChild postXml() {
		def requestArgs = this.args

		return this.post(requestArgs)
	}
	
	def setArgs(Map<String,?> args) {
		this.args = args
	}
}
