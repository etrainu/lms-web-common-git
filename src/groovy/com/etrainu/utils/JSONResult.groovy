package com.etrainu.utils

import grails.converters.JSON

class JSONResult {
	public String message
	public Boolean success
	
	def JSONResult() {
		this('OK.', true)
	}
	
	def JSONResult(String message) {
		this(message, true)
	}
	
	def JSONResult(Boolean success) {
		this('OK.', success)
	}
	
	def JSONResult(String message, Boolean success) {
		this.message = message
		this.success = success
	}
	
	public toJSON() {
		def json = this.toMap() as JSON
		
		//JSON is a string after all
		return json.toString()
	}
	
	public toMap() {
		return [message:this.message, success:this.success]
	}
}
