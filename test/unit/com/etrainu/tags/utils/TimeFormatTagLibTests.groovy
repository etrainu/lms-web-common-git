package com.etrainu.tags.utils

import com.etrainu.tags.utils.TimeFormatTagLib;

import grails.test.*

class TimeFormatTagLibTests extends TagLibUnitTestCase {
	def taglib
	private StringWriter output

	protected void setUp() {
		super.setUp()
		taglib = new TimeFormatTagLib()
		output = new StringWriter()
		taglib.metaClass.out = output
	}

	protected void tearDown() {
		super.tearDown()
	}

	void test10mAverageTime() {
		taglib.totalTime(time:10)
		assertEquals("10m", output.toString())
	}
	
	void test1hAverageTime() {
		taglib.totalTime(time:60)
		assertEquals("1h", output.toString())
	}
	
	void test1h15mAverageTime() {
		taglib.totalTime(time:75)
		assertEquals("1h 15m", output.toString())
	}
	
	void test1dAverageTime(){
		taglib.totalTime(time:1440)
		assertEquals("1d", output.toString())
	}
	
	void test2d15hAverageTime() {
		taglib.totalTime(time:3780)
		assertEquals("2d 15h", output.toString())
	}
	
	void test28d14h5mAverageTime() {
		taglib.totalTime(time:41165)
		assertEquals("28d 14h 5m", output.toString())
	}
	
	void testNullAverageTime() {
		taglib.totalTime(time:null)
		assertEquals("NA", output.toString())
	}

	void testNegatimeAverageTime() {
		taglib.totalTime(time:-10)
		assertEquals("NA", output.toString())
	}
	
	void test0AverageTime() {
		taglib.totalTime(time:0)
		assertEquals("NA", output.toString())
	}
	
	/*void testTextAverageTime() {
		taglib.totalTIme(time:"test")
		assertEquals("NA", output.toString())
	}*/

	void testIncorrectAverageTime() {
		assert ("20 Minutes" != taglib.totalTime(time:10))
	}
}
