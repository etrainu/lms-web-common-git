package com.etrainu.tags.utils

import grails.test.*

import java.io.StringWriter

import org.junit.After
import org.junit.Before
import org.junit.Test

class TextDisplayTagLibTests extends TagLibUnitTestCase {
    
	def taglib
	private StringWriter output
	
	@Before
	public void setUp() {
        super.setUp()	
		taglib = new TextDisplayTagLib()
		output = new StringWriter()
		taglib.metaClass.out = output
    }

	@After
    public void tearDown() {
        super.tearDown()
    }
	
	@Test
	void basicDefaultTest() {
		taglib.defaultText(value:null, default:"def")
		assertEquals "<span class=\"\">def</span>", output.toString()
	}
	
	@Test
	void basicEmptyTextTest() {
		taglib.defaultText(value:"", default:"def", defaultBlank:"true")
		assertEquals "<span class=\"\">def</span>", output.toString()
	}

	@Test
    void testSomething() {
		// value, default, defaultClass, acceptedClass, format, test
		taglib.defaultText(value:"test", default:"def", defaultClass:"defaultClass", acceptedClass:"accClass", format:"", test:"true")
		assertEquals "<span class=\"defaultClass\">def</span>", output.toString()			  
    }
}
