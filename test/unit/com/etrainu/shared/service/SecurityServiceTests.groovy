package com.etrainu.shared.service

import grails.test.*

class SecurityServiceTests extends GrailsUnitTestCase {
	
	def mockSpringSecurityService
	
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

	//Ensure the eTrainu credentials are still present
    void testEtrainuCredentials() {
		def service = new SecurityService()
		
		def credentialMap = service.getCredentials()
		assertTrue credentialMap instanceof LinkedHashMap
		assertTrue credentialMap.containsKey("u")
		assertTrue credentialMap.containsKey("p")
    }
}
